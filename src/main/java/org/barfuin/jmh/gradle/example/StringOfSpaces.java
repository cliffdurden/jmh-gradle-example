/*
 * Copyright 2021 the jmh-gradle-example contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.jmh.gradle.example;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;


/**
 * This class represents the "regular" code which you want to benchmark with your JMH tests.
 */
public final class StringOfSpaces
{
    /** a String with 177 spaces */
    private static final String SPACES_177 = "                                                                       "
        + "                                                                                                          ";



    private StringOfSpaces()
    {
        // utility class
    }



    public static String byStream(final int pLength)
    {
        return Stream.generate(() -> " ").limit(pLength).collect(Collectors.joining());
    }



    @SuppressWarnings("StringConcatenationInLoop")
    public static String bySubstring(final int pLength)
    {
        String result = "";
        for (int i = 1; i * SPACES_177.length() <= pLength; i++) {
            result += SPACES_177;
        }
        result += SPACES_177.substring(0, pLength % SPACES_177.length());
        return result;
    }



    public static String bySubstringOrStream(final int pLength)
    {
        if (pLength <= SPACES_177.length()) {
            return SPACES_177.substring(0, pLength);
        }
        return byStream(pLength);
    }



    @SuppressWarnings("StringRepeatCanBeUsed")
    public static String byStringBuilder(final int pLength)
    {
        StringBuilder sb = new StringBuilder(pLength + 1);
        for (int i = 0; i < pLength; i++) {
            sb.append(' ');
        }
        return sb.toString();
    }



    @SuppressWarnings("StringConcatenationInLoop")
    public static String byTrivialConcat(final int pLength)
    {
        String result = "";
        for (int i = 0; i < pLength; i++) {
            result += " ";
        }
        return result;
    }



    public static String byArrayFill(final int pLength)
    {
        if (pLength > 0) {
            char[] array = new char[pLength];
            Arrays.fill(array, ' ');
            return new String(array);
        }
        return "";
    }



    public static String byJava11Repeat(final int pLength)
    {
        return " ".repeat(pLength);
    }



    public static String byCommonsLang3(final int pLength)
    {
        return StringUtils.repeat(" ", pLength);
    }
}
