/*
 * Copyright 2021 the jmh-gradle-example contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.jmh.gradle.example;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link StringOfSpaces}, just so we can be sure that the functions tested are functionally correct.
 * <p>This has nothing to do with the performance testing.</p>
 */
public class StringOfSpacesTest
{
    @Test
    public void testBySubstring()
    {
        Assert.assertEquals(0, StringOfSpaces.bySubstring(0).length());
        Assert.assertEquals(1, StringOfSpaces.bySubstring(1).length());
        Assert.assertEquals(10, StringOfSpaces.bySubstring(10).length());
        Assert.assertEquals(200, StringOfSpaces.bySubstring(200).length());
        Assert.assertEquals(500, StringOfSpaces.bySubstring(500).length());
    }



    @Test
    public void testByStream()
    {
        Assert.assertEquals(0, StringOfSpaces.byStream(0).length());
        Assert.assertEquals(1, StringOfSpaces.byStream(1).length());
        Assert.assertEquals(10, StringOfSpaces.byStream(10).length());
        Assert.assertEquals(200, StringOfSpaces.byStream(200).length());
        Assert.assertEquals(500, StringOfSpaces.byStream(500).length());
    }



    @Test
    public void testBySubstringOrStream()
    {
        Assert.assertEquals(0, StringOfSpaces.bySubstringOrStream(0).length());
        Assert.assertEquals(1, StringOfSpaces.bySubstringOrStream(1).length());
        Assert.assertEquals(10, StringOfSpaces.bySubstringOrStream(10).length());
        Assert.assertEquals(200, StringOfSpaces.bySubstringOrStream(200).length());
        Assert.assertEquals(500, StringOfSpaces.bySubstringOrStream(500).length());
    }



    @Test
    public void testByStringBuilder()
    {
        Assert.assertEquals(0, StringOfSpaces.byStringBuilder(0).length());
        Assert.assertEquals(1, StringOfSpaces.byStringBuilder(1).length());
        Assert.assertEquals(10, StringOfSpaces.byStringBuilder(10).length());
        Assert.assertEquals(200, StringOfSpaces.byStringBuilder(200).length());
        Assert.assertEquals(500, StringOfSpaces.byStringBuilder(500).length());
    }



    @Test
    public void testByTrivialConcat()
    {
        Assert.assertEquals(0, StringOfSpaces.byTrivialConcat(0).length());
        Assert.assertEquals(1, StringOfSpaces.byTrivialConcat(1).length());
        Assert.assertEquals(10, StringOfSpaces.byTrivialConcat(10).length());
        Assert.assertEquals(200, StringOfSpaces.byTrivialConcat(200).length());
        Assert.assertEquals(500, StringOfSpaces.byTrivialConcat(500).length());
    }



    @Test
    public void testByArrayFill()
    {
        Assert.assertEquals(0, StringOfSpaces.byArrayFill(0).length());
        Assert.assertEquals(1, StringOfSpaces.byArrayFill(1).length());
        Assert.assertEquals(10, StringOfSpaces.byArrayFill(10).length());
        Assert.assertEquals(200, StringOfSpaces.byArrayFill(200).length());
        Assert.assertEquals(500, StringOfSpaces.byArrayFill(500).length());
    }



    @Test
    public void testByJava11Repeat()
    {
        Assert.assertEquals(0, StringOfSpaces.byJava11Repeat(0).length());
        Assert.assertEquals(1, StringOfSpaces.byJava11Repeat(1).length());
        Assert.assertEquals(10, StringOfSpaces.byJava11Repeat(10).length());
        Assert.assertEquals(200, StringOfSpaces.byJava11Repeat(200).length());
        Assert.assertEquals(500, StringOfSpaces.byJava11Repeat(500).length());
    }



    @Test
    public void testByCommonsLang3()
    {
        Assert.assertEquals(0, StringOfSpaces.byCommonsLang3(0).length());
        Assert.assertEquals(1, StringOfSpaces.byCommonsLang3(1).length());
        Assert.assertEquals(10, StringOfSpaces.byCommonsLang3(10).length());
        Assert.assertEquals(200, StringOfSpaces.byCommonsLang3(200).length());
        Assert.assertEquals(500, StringOfSpaces.byCommonsLang3(500).length());
    }
}
